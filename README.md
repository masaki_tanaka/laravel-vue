# Laravel + Vueサンプル

VueをBladeと置き換えたサンプルです

```
composer install
```

``` 
cp .env.example .env
```

```
php artisan package:discover && php artisan key:generate
```

``` 
npm install && npm run dev
```

``` 
php artisan serv
```

localhost:8000

storybook
```
npm run storybook
```